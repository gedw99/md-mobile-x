# Get Deps
#glide install

# Run Tests
go test .

gomobile build -target=android

gomobile build -target=ios

# IOS verbose build
#gomobile build -target=ios -x