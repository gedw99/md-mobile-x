package main

import "golang.org/x/mobile/app"
import "fmt"
import "strconv"

func main() {
	app.Main(func(a app.App) {
		fmt.Println("main ...")

		fmt.Println("sum 1 + 2 = " + strconv.Itoa(Sum(1, 2)))
	})
}

// Sum returns the sum of a and b
// Use GoSampleSum to call it from objective-c
func Sum(a int, b int) (result int) {
	return a + b
}
