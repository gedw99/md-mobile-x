# md-mobile-x repo

<https://bitbucket.org/gedw99/md-mobile-x>


This repo is to test out certain things with Golang mobile. 

The amin is to have a Polymer web fronttend, with a golang based backend.
- Wrapped using cordova
- Dont knwo if gobind is needed or not yet


## Help

https://groups.google.com/forum/#!forum/go-mobile


## Test Projects

gomobilecheck is a gomobile app, built with the gomobile tool. 
- This Project tests a standard Android & IOS, and is used to ensure that the basic Compile & Run works. 
- glide install to get all vendored golang packages
- Just has a blank screen & does basic math.
- You should be able to run in Simulator and on your Android & Iphone.

ios_test01 is standard ios app, build with xcode.
- This project tests a standard IOS Swift app, and is used to ensure that the basic Compile & Run works. 
- Just has a blank screen. 
- You should be able to run in Simulator and on your Iphone. 



## Setup

- golang 1.7
    - <https://golang.org/dl/>
    - Setup your bash profile correctly:
        ````
            ### GoLang 1.5 setup ###
            export GOROOT=/usr/local/go         # this assumes the go runtime has been downloaded and placed in “/usr/local/go” 
            export GO15VENDOREXPERIMENT=1 
            export GOPATH=$HOME/workspace/go 
            export GOBIN=$GOPATH/bin 
            export PATH=$PATH:$GOROOT/bin:$GOBIN 
        ````
- gomobile
    - <https://github.com/golang/go/wiki/Mobile#tools>
    - ```go get golang.org/x/mobile/cmd/gomobile ```
    
- Glide 
    - <https://github.com/Masterminds/glide#install>


## Test Devices

- iphone passcode: 4934
- android pin: 5040



